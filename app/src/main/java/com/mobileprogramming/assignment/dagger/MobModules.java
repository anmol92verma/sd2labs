package com.mobileprogramming.assignment.dagger;

import com.mobileprogramming.assignment.utils.GlobalVariables;
import com.mobileprogramming.assignment.core.MobApplication;
import com.mobileprogramming.assignment.network.MobService;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by anmol on 23/7/16.
 */
@Module
public class MobModules {
    private final MobApplication mobApplication;

    public MobModules(MobApplication mobApplication) {
        this.mobApplication = mobApplication;
    }

    @Provides
    @Singleton
    @Named(GlobalVariables.Globals.MAIN_THREAD)
    Scheduler provideMainThreadScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    @Named(GlobalVariables.Globals.NEW_THREAD)
    Scheduler provideNewThreadScheduler() {
        return Schedulers.newThread();
    }

    @Provides
    @Named(GlobalVariables.Globals.UNAUTHORIZED)
    @Singleton
    MobService providesMobService() {
        final long NETWORK_TIMEOUT = 100;
        final long READ_TIMEOUT = 10;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GlobalVariables.APIS.DOMAIN)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        MobService service = retrofit.create(MobService.class);
        return service;
    }
}
