package com.mobileprogramming.assignment.dagger;

import com.mobileprogramming.assignment.mvp.views.SplashActivity;
import com.mobileprogramming.assignment.mvp.views.userlisting.UsersListActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by anmol on 23/7/16.
 */

@Component(modules = MobModules.class)
@Singleton
public interface MobComponents {
    void inject(UsersListActivity mainActivity);

    void inject(SplashActivity splashActivity);
}
