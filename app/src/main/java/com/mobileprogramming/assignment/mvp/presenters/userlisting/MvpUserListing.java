package com.mobileprogramming.assignment.mvp.presenters.userlisting;

import com.mobileprogramming.assignment.core.basemvp.MvpView;
import com.mobileprogramming.assignment.mvp.models.ResponseUsersList;

/**
 * Created by anmol on 23/7/16.
 */

public interface MvpUserListing extends MvpView {
    void successFeedsList(ResponseUsersList responseUserInterests);

}
