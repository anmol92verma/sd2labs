package com.mobileprogramming.assignment.mvp.presenters.userlisting;

import com.mobileprogramming.assignment.core.basemvp.BasePresenter;
import com.mobileprogramming.assignment.mvp.models.RequestPaginationModel;
import com.mobileprogramming.assignment.mvp.models.ResponseUsersList;
import com.mobileprogramming.assignment.network.MobService;
import com.mobileprogramming.assignment.utils.GlobalVariables;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Scheduler;
import rx.Subscriber;

/**
 * Created by anmol on 23/7/16.
 */

public class UsersListPresenter extends BasePresenter<MvpUserListing> {

    @Inject
    @Named(GlobalVariables.Globals.UNAUTHORIZED)
    MobService service;

    @Inject
    @Named(GlobalVariables.Globals.MAIN_THREAD)
    public Scheduler mainThread;

    @Inject
    @Named(GlobalVariables.Globals.NEW_THREAD)
    public Scheduler newThread;

    @Inject
    public UsersListPresenter() {

    }

    @Override
    public void attachView(MvpUserListing mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public void getFeedsList(RequestPaginationModel apiPagingRequest) {
        if (isViewAttached()) {
            getMvpView().showProgress();
            int limit = apiPagingRequest.getLimit();
            int offset = apiPagingRequest.getOffset();
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(GlobalVariables.Globals.LIMIT, String.valueOf(limit));
            hashMap.put(GlobalVariables.Globals.OFFSET, String.valueOf(offset));

            service.getUsersList(hashMap)
                    .subscribeOn(newThread)
                    .observeOn(mainThread)
                    .subscribe(new Subscriber<ResponseUsersList>() {
                        @Override
                        public void onCompleted() {
                            if (isViewAttached())
                                getMvpView().hideProgress();
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (isViewAttached())
                                getMvpView().showError(e);
                        }

                        @Override
                        public void onNext(ResponseUsersList responseUsersList) {
                            if (isViewAttached()) {
                                getMvpView().successFeedsList(responseUsersList);
                            }
                        }
                    });
        }
    }
}
