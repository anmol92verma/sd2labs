package com.mobileprogramming.assignment.mvp.models;

import java.util.List;

/**
 * Created by anmol on 23/7/16.
 */
public class ResponseUsersList {

    /**
     * status : true
     * data : {"users":[{"name":"User-11","image":"http://loremflickr.com/300/300?random=11","items":["http://loremflickr.com/800/800?random=38","http://loremflickr.com/800/800?random=43","http://loremflickr.com/800/800?random=15","http://loremflickr.com/800/800?random=16","http://loremflickr.com/800/800?random=19"]},{"name":"User-12","image":"http://loremflickr.com/300/300?random=12","items":["http://loremflickr.com/800/800?random=49","http://loremflickr.com/800/800?random=34","http://loremflickr.com/800/800?random=37","http://loremflickr.com/800/800?random=13","http://loremflickr.com/800/800?random=20"]},{"name":"User-13","image":"http://loremflickr.com/300/300?random=13","items":["http://loremflickr.com/800/800?random=16","http://loremflickr.com/800/800?random=21"]},{"name":"User-14","image":"http://loremflickr.com/300/300?random=14","items":["http://loremflickr.com/800/800?random=22","http://loremflickr.com/800/800?random=16","http://loremflickr.com/800/800?random=8","http://loremflickr.com/800/800?random=33"]},{"name":"User-15","image":"http://loremflickr.com/300/300?random=15","items":["http://loremflickr.com/800/800?random=10","http://loremflickr.com/800/800?random=41"]},{"name":"User-16","image":"http://loremflickr.com/300/300?random=16","items":["http://loremflickr.com/800/800?random=5","http://loremflickr.com/800/800?random=24","http://loremflickr.com/800/800?random=43","http://loremflickr.com/800/800?random=39"]},{"name":"User-17","image":"http://loremflickr.com/300/300?random=17","items":["http://loremflickr.com/800/800?random=11","http://loremflickr.com/800/800?random=26","http://loremflickr.com/800/800?random=48"]},{"name":"User-18","image":"http://loremflickr.com/300/300?random=18","items":["http://loremflickr.com/800/800?random=24","http://loremflickr.com/800/800?random=3","http://loremflickr.com/800/800?random=14","http://loremflickr.com/800/800?random=10","http://loremflickr.com/800/800?random=19"]},{"name":"User-19","image":"http://loremflickr.com/300/300?random=19","items":["http://loremflickr.com/800/800?random=1","http://loremflickr.com/800/800?random=46"]},{"name":"User-20","image":"http://loremflickr.com/300/300?random=20","items":["http://loremflickr.com/800/800?random=25","http://loremflickr.com/800/800?random=5","http://loremflickr.com/800/800?random=10","http://loremflickr.com/800/800?random=13","http://loremflickr.com/800/800?random=36"]}],"has_more":true}
     */

    private boolean status;
    /**
     * users : [{"name":"User-11","image":"http://loremflickr.com/300/300?random=11","items":["http://loremflickr.com/800/800?random=38","http://loremflickr.com/800/800?random=43","http://loremflickr.com/800/800?random=15","http://loremflickr.com/800/800?random=16","http://loremflickr.com/800/800?random=19"]},{"name":"User-12","image":"http://loremflickr.com/300/300?random=12","items":["http://loremflickr.com/800/800?random=49","http://loremflickr.com/800/800?random=34","http://loremflickr.com/800/800?random=37","http://loremflickr.com/800/800?random=13","http://loremflickr.com/800/800?random=20"]},{"name":"User-13","image":"http://loremflickr.com/300/300?random=13","items":["http://loremflickr.com/800/800?random=16","http://loremflickr.com/800/800?random=21"]},{"name":"User-14","image":"http://loremflickr.com/300/300?random=14","items":["http://loremflickr.com/800/800?random=22","http://loremflickr.com/800/800?random=16","http://loremflickr.com/800/800?random=8","http://loremflickr.com/800/800?random=33"]},{"name":"User-15","image":"http://loremflickr.com/300/300?random=15","items":["http://loremflickr.com/800/800?random=10","http://loremflickr.com/800/800?random=41"]},{"name":"User-16","image":"http://loremflickr.com/300/300?random=16","items":["http://loremflickr.com/800/800?random=5","http://loremflickr.com/800/800?random=24","http://loremflickr.com/800/800?random=43","http://loremflickr.com/800/800?random=39"]},{"name":"User-17","image":"http://loremflickr.com/300/300?random=17","items":["http://loremflickr.com/800/800?random=11","http://loremflickr.com/800/800?random=26","http://loremflickr.com/800/800?random=48"]},{"name":"User-18","image":"http://loremflickr.com/300/300?random=18","items":["http://loremflickr.com/800/800?random=24","http://loremflickr.com/800/800?random=3","http://loremflickr.com/800/800?random=14","http://loremflickr.com/800/800?random=10","http://loremflickr.com/800/800?random=19"]},{"name":"User-19","image":"http://loremflickr.com/300/300?random=19","items":["http://loremflickr.com/800/800?random=1","http://loremflickr.com/800/800?random=46"]},{"name":"User-20","image":"http://loremflickr.com/300/300?random=20","items":["http://loremflickr.com/800/800?random=25","http://loremflickr.com/800/800?random=5","http://loremflickr.com/800/800?random=10","http://loremflickr.com/800/800?random=13","http://loremflickr.com/800/800?random=36"]}]
     * has_more : true
     */

    private DataBean data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private boolean has_more;
        /**
         * name : User-11
         * image : http://loremflickr.com/300/300?random=11
         * items : ["http://loremflickr.com/800/800?random=38","http://loremflickr.com/800/800?random=43","http://loremflickr.com/800/800?random=15","http://loremflickr.com/800/800?random=16","http://loremflickr.com/800/800?random=19"]
         */

        private List<UsersBean> users;

        public boolean isHas_more() {
            return has_more;
        }

        public void setHas_more(boolean has_more) {
            this.has_more = has_more;
        }

        public List<UsersBean> getUsers() {
            return users;
        }

        public void setUsers(List<UsersBean> users) {
            this.users = users;
        }

        public static class UsersBean {
            private String name;
            private String image;
            private List<String> items;
            private int type;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public List<String> getItems() {
                return items;
            }

            public void setItems(List<String> items) {
                this.items = items;
            }

            public void setType(int type) {
                this.type = type;
            }

            public int getType() {
                return type;
            }
        }
    }
}
