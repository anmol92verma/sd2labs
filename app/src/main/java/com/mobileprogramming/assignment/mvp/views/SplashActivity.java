package com.mobileprogramming.assignment.mvp.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.mobileprogramming.assignment.core.MobActivity;
import com.mobileprogramming.assignment.core.MobApplication;
import com.mobileprogramming.assignment.mvp.views.userlisting.UsersListActivity;
import com.mobileprogramming.assignment.utils.GlobalVariables;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by anmol on 23/7/16.
 */

public class SplashActivity extends MobActivity {
    @Inject
    @Named(GlobalVariables.Globals.MAIN_THREAD)
    Scheduler mainThreadScheduler;
    private Subscription subscription;
    private SplashActivity context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
    }

    @Override
    protected boolean isFullScreen() {
        return true;
    }

    @Override
    protected boolean shouldAttachFont() {
        return true;
    }

    @Override
    public void injectDependency() {
        getMobApplication().getMobComponent().inject(this);
    }

    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        subscription = Observable.
                timer(1500, TimeUnit.MILLISECONDS).
                observeOn(mainThreadScheduler).
                subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        startActivity(new Intent(context, UsersListActivity.class));
                        finish();
                        unsubscribeIfNotNull(subscription);
                    }
                });
    }


    @Override
    protected void onPause() {
        super.onPause();
        unsubscribeIfNotNull(subscription);
    }

    private void unsubscribeIfNotNull(Subscription subscription) {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }
}
