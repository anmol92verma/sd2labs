package com.mobileprogramming.assignment.mvp.models;

/**
 * Created by anmol on 23/7/16.
 */

public class RequestPaginationModel {
    /**
     * limit : 1
     * Offset : 2
     */

    private int limit;
    private int Offset;

    public int getLimit() {
        return limit;
    }

    public void setLimit(int PageNum) {
        this.limit = PageNum;
    }

    public int getOffset() {
        return Offset;
    }

    public void setOffset(int Offset) {
        this.Offset = Offset;
    }
}
