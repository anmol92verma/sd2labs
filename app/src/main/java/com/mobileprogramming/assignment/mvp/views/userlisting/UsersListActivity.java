package com.mobileprogramming.assignment.mvp.views.userlisting;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.mobileprogramming.assignment.R;
import com.mobileprogramming.assignment.core.MobActivity;
import com.mobileprogramming.assignment.customviews.FeedItemAnimator;
import com.mobileprogramming.assignment.mvp.models.RequestPaginationModel;
import com.mobileprogramming.assignment.mvp.models.ResponseUsersList;
import com.mobileprogramming.assignment.mvp.presenters.userlisting.MvpUserListing;
import com.mobileprogramming.assignment.mvp.presenters.userlisting.UsersListPresenter;
import com.mobileprogramming.assignment.utils.PaginatedOnScrollListener;

import javax.inject.Inject;

import butterknife.BindView;

public class UsersListActivity extends MobActivity implements MvpUserListing,
        SwipeRefreshLayout.OnRefreshListener,
        PaginatedOnScrollListener.PaginationListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerViewFeeds;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshFeeds;

    @Inject
    PaginatedOnScrollListener paginatedOnScrollListener;

    @Inject
    UsersListPresenter presenter;

    @Inject
    AdapterFeeds adapterFeeds;
    private boolean isLoading;
    private int mOffset = 0;
    private boolean moreItems = true;

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean shouldAttachFont() {
        return true;
    }

    @Override
    public void injectDependency() {
        getMobApplication().getMobComponent().inject(this);
    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView(this);
        toolbar.setTitleTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        toolbar.setSubtitleTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        toolbar.setTitle(getString(R.string.app_name));
        toolbar.setSubtitle(getString(R.string.myname));
        recyclerViewFeeds.setLayoutManager(super.getLinearLayoutManager());
        recyclerViewFeeds.setItemAnimator(new FeedItemAnimator());
        recyclerViewFeeds.setAdapter(adapterFeeds);
        paginatedOnScrollListener.setPaginationListener(this);
        recyclerViewFeeds.addOnScrollListener(paginatedOnScrollListener);
        swipeRefreshFeeds.setOnRefreshListener(this);
        getFeeds();
    }

    @Override
    public SwipeRefreshLayout getSwipeRefresh() {
        return this.swipeRefreshFeeds;
    }

    @Override
    public void getFeeds() {
        isLoading = true;
        RequestPaginationModel apiPagingRequest = new RequestPaginationModel();
        apiPagingRequest.setOffset(mOffset);
        apiPagingRequest.setLimit(10);
        presenter.getFeedsList(apiPagingRequest);
    }

    @Override
    public RecyclerView.Adapter getAdapter() {
        return adapterFeeds;
    }

    @Override
    public void getMoreFeeds() {
        if (moreItems) {
            if (!isLoading) {
                mOffset += 10;
                getFeeds();
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }


    @Override
    public void showProgress() {
        isLoading = true;
        if (mOffset == 0) {
            swipeRefreshFeeds.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshFeeds.setRefreshing(true);
                }
            });
        } else {
            adapterFeeds.addMoreProgress();
        }
    }

    @Override
    public void hideProgress() {
        isLoading = false;
        swipeRefreshFeeds.setRefreshing(false);
    }

    @Override
    public void showError(Exception arg0) {
        hideProgressViews();
        super.showSnackBar(arg0.getMessage());
    }

    @Override
    public void showError(Throwable e) {
        hideProgressViews();
        super.showSnackBar(e.getMessage());
    }

    private void hideProgressViews() {
        swipeRefreshFeeds.setRefreshing(false);
    }


    @Override
    public void successFeedsList(ResponseUsersList responseUsersList) {
        if (mOffset > 1) {
            adapterFeeds.addDataSet(responseUsersList.getData().getUsers());
        } else {
            adapterFeeds.setDataSet(responseUsersList.getData().getUsers());
        }

        if (!responseUsersList.getData().isHas_more()) {
            moreItems = false;
        } else {
            moreItems = true;
        }

    }

    @Override
    public void onRefresh() {
        mOffset = 0;
        if (adapterFeeds != null) {
            adapterFeeds.clear();
        }
        if (!isLoading) {
            getFeeds();
        } else {
            getSwipeRefresh().setRefreshing(false);
        }
    }

}
