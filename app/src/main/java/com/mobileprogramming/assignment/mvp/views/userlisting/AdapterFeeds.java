package com.mobileprogramming.assignment.mvp.views.userlisting;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mobileprogramming.assignment.R;
import com.mobileprogramming.assignment.customviews.CropCircleTransformation;
import com.mobileprogramming.assignment.customviews.SquaredImageView;
import com.mobileprogramming.assignment.mvp.models.ResponseUsersList;
import com.mobileprogramming.assignment.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.http.POST;

/**
 * Created by anmol on 23/7/16.
 */

public class AdapterFeeds extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ResponseUsersList.DataBean.UsersBean> dataSet;
    CropCircleTransformation transformation;
    private String COUNT = "%s images";
    private final int NORMAL = 0;
    private final int PROGRESS = 1;

    @Inject
    public AdapterFeeds() {
        dataSet = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        transformation = new CropCircleTransformation(parent.getContext());

        switch (viewType) {
            case NORMAL:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_itemfeeds, parent, false);
                return new ViewHolder(view);
            case PROGRESS:
                View viewProgress = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_progress, parent, false);
                return new ViewHolderProgress(viewProgress);
            default:
                return null;
        }

    }


    @Override
    public int getItemViewType(int position) {
        return dataSet.get(position).getType() == 0 ? NORMAL : PROGRESS;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder holders = (ViewHolder) holder;
            holders.bindUI();
        }

    }

    private void loadImage(SquaredImageView squaredImageView, String url) {

        Glide.with(squaredImageView.getContext()).load(url)
                .thumbnail(0.1f)
                .error(R.drawable.bg_like_square_background)
                .placeholder(R.drawable.bg_like_square_background)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(squaredImageView);
    }

    private ResponseUsersList.DataBean.UsersBean getItemData(int adapterPosition) {
        return dataSet.get(adapterPosition);
    }

    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.size();
    }

    public void clear() {
        this.dataSet.clear();
        notifyDataSetChanged();
    }

    public void addDataSet(List<ResponseUsersList.DataBean.UsersBean> users) {
        hideMoreProgress();
        int positionOfLastItem = this.dataSet.size() + 1;
        this.dataSet.addAll(users);
        notifyItemRangeInserted(positionOfLastItem, users.size());
    }

    public void setDataSet(List<ResponseUsersList.DataBean.UsersBean> dataSet) {
        this.dataSet = dataSet;
        notifyItemRangeRemoved(0, this.dataSet.size());
    }

    public void addMoreProgress() {
        ResponseUsersList.DataBean.UsersBean moreProgress = new ResponseUsersList.DataBean.UsersBean();
        moreProgress.setType(1);
        this.dataSet.add(moreProgress);
        notifyItemInserted(this.dataSet.size());
    }

    public void hideMoreProgress() {
        int positionRemoved = dataSet.size() - 1;
        this.dataSet.remove(positionRemoved);
        notifyItemRemoved(positionRemoved);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivUserProfile)
        ImageView ivUserProfile;
        @BindView(R.id.vImageRoot)
        FrameLayout vImageRoot;
        @BindView(R.id.card_view)
        CardView cardView;
        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.tvImgCount)
        TextView tvImgCount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindUI() {
            ResponseUsersList.DataBean.UsersBean dataItem = getItemData(getAdapterPosition());
            String userName = dataItem.getName();
            tvUserName.setText(TextUtils.isEmpty(userName) ? "" : userName);
            String userImage = dataItem.getImage();
            Glide.with(itemView.getContext()).load(userImage)
                    .thumbnail(0.1f)
                    .error(R.drawable.bg_like_circle_background)
                    .bitmapTransform(transformation)
                    .placeholder(R.drawable.bg_like_circle_background)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ivUserProfile);
            List<String> feedImages = dataItem.getItems();

            if (feedImages != null && !feedImages.isEmpty()) {
                tvImgCount.setText(String.format(COUNT, feedImages.size()));
                FrameLayout mMainlayout = vImageRoot;
                mMainlayout.removeAllViews();

                LinearLayout mPrimaryLayout = new LinearLayout(itemView.getContext());
                mPrimaryLayout.setOrientation(LinearLayout.VERTICAL);

                LinearLayout mSecondaryLayout = new LinearLayout(itemView.getContext());
                mSecondaryLayout.setOrientation(LinearLayout.HORIZONTAL);
                if (feedImages.size() % 2 != 0) {
                    // Odd
                    for (int i = 0; i < feedImages.size(); i++) {

                        if (i == 0) {
                            // First Item
                            SquaredImageView squaredImageView = new SquaredImageView(itemView.getContext());
                            mPrimaryLayout.addView(squaredImageView);
                            loadImage(squaredImageView, feedImages.get(i));

                            int width = squaredImageView.getContext().getResources().getDisplayMetrics().widthPixels;
                            int height = squaredImageView.getContext().getResources().getDisplayMetrics().heightPixels;

                            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) squaredImageView.getLayoutParams();
                            layoutParams.width = width;
                            layoutParams.height = height / 2;
                            layoutParams.setMargins(0, 0, 0, 8);
                            squaredImageView.setLayoutParams(layoutParams);

                        } else {
                            // Other Items
                            SquaredImageView squaredImageView = new SquaredImageView(itemView.getContext());

                            // if secondary linear layout contains just one image
                            if (mSecondaryLayout.getChildCount() == 1) {
                                // add another image and add to PrimaryLayout
                                mSecondaryLayout.addView(squaredImageView);
                                loadImage(squaredImageView, feedImages.get(i));

                                mPrimaryLayout.addView(mSecondaryLayout);


                                int width = squaredImageView.getContext().getResources().getDisplayMetrics().widthPixels;

                                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) squaredImageView.getLayoutParams();
                                layoutParams.width = (width / 2) - (Utils.dpToPx(12));
                                layoutParams.height = width / 2;
                                layoutParams.weight = 1;
                                if (i == feedImages.size() - 1) {
                                    layoutParams.setMargins(8, 8, 0, 0);

                                } else {
                                    layoutParams.setMargins(8, 8, 0, 8);
                                }
                                squaredImageView.setLayoutParams(layoutParams);

                                // next time it should add a new mSecondaryLayout
                                if (i < feedImages.size() - 1) {
                                    mSecondaryLayout = new LinearLayout(itemView.getContext());
                                }
                            } else {
                                // if secondary linear layout contains no image
                                mSecondaryLayout.addView(squaredImageView);
                                loadImage(squaredImageView, feedImages.get(i));

                                int width = squaredImageView.getContext().getResources().getDisplayMetrics().widthPixels;

                                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) squaredImageView.getLayoutParams();
                                layoutParams.width = (width / 2) - (Utils.dpToPx(12));
                                layoutParams.height = width / 2;
                                layoutParams.weight = 1;
                                layoutParams.setMargins(0, 8, 8, 8);
                                squaredImageView.setLayoutParams(layoutParams);

                            }
                        }
                    }

                } else {
                    // even
                    for (int i = 0; i < feedImages.size(); i++) {
                        // Other Items
                        SquaredImageView squaredImageView = new SquaredImageView(itemView.getContext());

                        // if secondary linear layout contains just one image
                        if (mSecondaryLayout.getChildCount() == 1) {
                            // add another image and add to PrimaryLayout
                            mSecondaryLayout.addView(squaredImageView);
                            loadImage(squaredImageView, feedImages.get(i));


                            mPrimaryLayout.addView(mSecondaryLayout);

                            int width = squaredImageView.getContext().getResources().getDisplayMetrics().widthPixels;

                            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) squaredImageView.getLayoutParams();
                            layoutParams.width = (width / 2) - (Utils.dpToPx(12));
                            layoutParams.weight = 1;
                            layoutParams.height = width / 2;
                            if (i == feedImages.size() - 1) {
                                layoutParams.setMargins(8, 8, 0, 0);
                            } else {
                                layoutParams.setMargins(8, 8, 0, 8);
                            }
                            squaredImageView.setLayoutParams(layoutParams);

                            // next time it should add a new mSecondaryLayout
                            if (i < feedImages.size() - 1) {
                                mSecondaryLayout = new LinearLayout(itemView.getContext());
                            }
                        } else {
                            // if secondary linear layout contains no image
                            mSecondaryLayout.addView(squaredImageView);

                            int width = squaredImageView.getContext().getResources().getDisplayMetrics().widthPixels;

                            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) squaredImageView.getLayoutParams();
                            layoutParams.width = (width / 2) - (Utils.dpToPx(12));
                            layoutParams.height = width / 2;
                            layoutParams.weight = 1;
                            layoutParams.setMargins(0, 8, 8, 8);
                            squaredImageView.setLayoutParams(layoutParams);
                            loadImage(squaredImageView, feedImages.get(i));

                        }
                    }

                }

                mMainlayout.addView(mPrimaryLayout);
            }
        }
    }

    public class ViewHolderProgress extends RecyclerView.ViewHolder {

        public ViewHolderProgress(View itemView) {
            super(itemView);
        }
    }
}
