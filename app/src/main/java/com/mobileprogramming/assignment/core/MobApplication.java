package com.mobileprogramming.assignment.core;

import android.app.Application;

import com.mobileprogramming.assignment.R;
import com.mobileprogramming.assignment.dagger.DaggerMobComponents;
import com.mobileprogramming.assignment.dagger.MobComponents;
import com.mobileprogramming.assignment.dagger.MobModules;

import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by anmol on 23/7/16.
 */

public class MobApplication extends Application {
    MobComponents mobComponents;

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        mobComponents = DaggerMobComponents.builder().mobModules(new MobModules(this)).build();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("DroidSans.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public MobComponents getMobComponent() {
        return mobComponents;
    }
}
