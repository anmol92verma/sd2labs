package com.mobileprogramming.assignment.utils;

/**
 * Created by anmol on 23/7/16.
 */
public class GlobalVariables {

    public interface Globals {
        String MAIN_THREAD = "mainThread";
        String NEW_THREAD = "newThread";
        String UNAUTHORIZED = "unAuthorized";
        String LIMIT = "limit";
        String OFFSET = "offset";
    }

    public interface APIS {
        String API_VERSION = "api/";
        String DOMAIN = "http://sd2-hiring.herokuapp.com/" + API_VERSION;
        String API_USERS_LIST = "users";
    }
}
