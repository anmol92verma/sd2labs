package com.mobileprogramming.assignment.utils;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import javax.inject.Inject;

/**
 * Created by anmol on 23/7/16.
 */
public class PaginatedOnScrollListener extends RecyclerView.OnScrollListener {


    private PaginationListener paginationListener;

    @Inject
    public PaginatedOnScrollListener() {

    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        if (newState == RecyclerView.SCROLL_STATE_IDLE &&
                paginationListener.getAdapter() != null &&
                paginationListener.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPositions = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
            if (lastVisibleItemPositions == paginationListener.getAdapter().getItemCount() - 1) {
                paginationListener.getMoreFeeds();
            }
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

    }

    public void setPaginationListener(PaginationListener paginationListener) {
        this.paginationListener = paginationListener;
    }

    public interface PaginationListener {
        SwipeRefreshLayout getSwipeRefresh();

        void getFeeds();

        RecyclerView.Adapter getAdapter();

        void getMoreFeeds();
    }
}
