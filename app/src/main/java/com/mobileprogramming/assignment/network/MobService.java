package com.mobileprogramming.assignment.network;

import com.mobileprogramming.assignment.utils.GlobalVariables;
import com.mobileprogramming.assignment.mvp.models.ResponseUsersList;

import java.util.HashMap;

import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by anmol on 23/7/16.
 */

public interface MobService {
    @GET(GlobalVariables.APIS.API_USERS_LIST)
    Observable<ResponseUsersList> getUsersList(@QueryMap HashMap<String, String> hashMap);
}
